//view identification
const jokesDiv = document.querySelector('#displayJoke');
const button = document.querySelector('#getJoke');

button.addEventListener('click', function(){
       // e.preventDefault();
       getRandomJoke();
})

//getting jokes
function getRandomJoke(){
	//ajax
	const ajax = new XMLHttpRequest;
	const url = 'https://api.chucknorris.io/jokes/random'
	//opening the request
	ajax.open('GET',url,true);
	//monitoring the request 
	ajax.onreadystatechange = function(){
		if (this.readyState === 4 && this.status === 200) {
            console.log(this.responseText);
            //converting the responsetext to be a js object
            let data = JSON.parse(this.responseText);
            jokesDiv.innerHTML = `${data.value}` + " " + `${data.url}`;   
		} else{
               this.onerror = onerror();
               console.log(onerror());
		}
	}
	ajax.send();
}

function onerror(){
	jokesDiv.innerHTML = "an error occurred";
}












