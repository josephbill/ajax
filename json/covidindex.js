const btn = document.getElementById("btn");
btn.addEventListener("click", function(){
	getCountry(getData);
});
//getCountry function 
function getCountry(callback){
	const url = `https://api.covid19api.com/summary`;
	const ajax = new XMLHttpRequest();
	ajax.open("GET",url,true);
	ajax.onload = function() {
    if (this.status === 200) {
      callback(this.responseText, showData);
    } else {
      console.log(this.statusText);
    }
  };
	// ajax.onreadystatechange = function(){
	// 	if (this.readystate === 4 && this.status === 200) {
 //              callback(this.responseText, showData);
	// 	} else {
	// 		//checking status error
	// 		console.log(this.statusText);
	// 	}
	// };
	ajax.onerror = function(){
       console.log("error with ajax request ")
	};
	ajax.send();
}
//get data : this is called, when get country is called
function getData(response,callback){
	const data = JSON.parse(response);
	//using destructuring to pull the data from the response 
     //destructuring is a js
     // syntax expression that makes it possible 
     //to uinpack values from arrays or properties from objects 
      //into distinct varibable
      // const{
      // 	Country,
      // 	TotalConfirmed,
      //   TotalRecovered
      // } = data.Countries[0];
      //loop to get all objects
        for (let i = 0; i < data.Countries.length; i++) {
           const{
      	Country,
      	TotalConfirmed,
        TotalRecovered
      } = data.Countries[i];
      console.log(data.Countries[i]) ;
         //callback
       callback(Country,TotalConfirmed,TotalRecovered); 
        }
    

} 
//show data
function showData(Country,TotalConfirmed,TotalRecovered){
	document.getElementById("country").textContent = Country;
	document.getElementById("casesConfirmed").textContent = TotalConfirmed;
	document.getElementById("recoveries").textContent = TotalRecovered;
}










