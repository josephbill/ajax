<!DOCTYPE html>
<html>
<head>
	<title>Form Post</title>
	  <meta charset="utf-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/css/bootstrap.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.5.1/jquery.min.js"></script>
  <script src="https://cdnjs.cloudflare.com/ajax/libs/popper.js/1.16.0/umd/popper.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.5.2/js/bootstrap.min.js"></script>
</head>
<body>
   <br>
   <br>
	<div class="container">
		<form id="login" enctype="multipart/form-data" method="post">
			<div class="form-group">
			<input type="text" name="username" id="username" placeholder="Name" class="form-control">
		</div>
		<div class="form-group">
			<input placeholder="Enter Job" type="text" name="job" id="job" class="form-control">
		</div>
		<div class="form-group">
			<input type="submit" name="save" id="save" class="btn btn-primary">
		</div>
		</form>
	
	</div>
    
<script type="text/javascript" src="posttojson.js"></script>    
</body>
</html>
