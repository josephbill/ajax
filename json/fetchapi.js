//define an array to hold our objects
let countries = []
//use fetch api to consume to json
fetch("https://api.covid19api.com/summary")
     .then(data => data.json())
     .then(data => {
     	for(country of data.Countries){
     		countries.push(
                  `<li>
                     <h1>${country.Country}</h1>
                     <p>${country.TotalConfirmed}</p>
                     <p>${country.TotalRecovered}</p>
                  </li>`
     			);
     	}
     	document
     	.getElementById("test").innerHTML = countries.join("");
     })



