document.getElementById("login").addEventListener("submit",function(e){
           e.preventDefault();
           //using a form data object 
           //the FormData object will be populated with the form's current keys/values
           // using the name property of each element for the keys and 
           //their submitted value for the values. It will also encode file input content.
           //formData sends the values as one 
           const formData = new FormData(this);
           //we can send values as parameters also using the 
           //URLSearchParams object 
           const searchParams = new URLSearchParams();
           //loop through contents of formdata to submit data
           //as key, value pairs
           for(const pair of formData){
            //this loop runs twice , the loop's end is determined by content in the form
            //data
            //pair refers to the key and values i.e 0 refers to the input
            //1 refers to the value of the input
             searchParams.append(pair[0],pair[1])
           }

           //fetching 
           fetch('fetchpostone.php',{
           	  method: 'post',
           	  body: formData
           }).then(function(response){
               return response.text();
           }).then(function(text){
           	console.log(text);
           }).catch(function(error){
           	 console.error(error);
           });


})