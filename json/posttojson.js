document.getElementById("login").addEventListener("submit", function(e){
	e.preventDefault();
	//capture users input
	  var username = document.getElementById("username").value;
           var job = document.getElementById("job").value;
  //api post 
fetch('https://reqres.in/api/users', {
  method: 'POST',
  headers: {
    'Content-Type': 'application/json',
  },
   body: JSON.stringify({
     // your expected POST request payload goes here
     //first part is the parameter , second part is the value
      name: username,
      job: job
      })
})
  .then(res => res.json())
  .then(data => {
   // enter you logic when the fetch is successful
    console.log(data)
    //here u can do a success alert
  })
  .catch(error => {
  // enter your logic for when there is an error (ex. error toast)
   console.log(error)
   //here u can show user the error.
  })  
})